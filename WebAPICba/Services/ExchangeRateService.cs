﻿using System.Globalization;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore;
using WebAPICba.Data;
using WebAPICba.Gate;
using WebAPICba.Models;
using static System.Runtime.InteropServices.JavaScript.JSType;
using ExchangeRate = WebAPICba.Models.ExchangeRate;

namespace WebAPICba.Services;

public class ExchangeRateService
{
    private readonly ExchangeRateContext _context;
    private readonly GateSoapClient _client;

    public ExchangeRateService(ExchangeRateContext context, GateSoapClient client)
    {
        _context = context;
        _client = client;
    }

    public async Task<List<ExchangeRateDTO>> GetExchangeRates(DateTime startDate, DateTime endDate, string[] isoCodes)
    {
        if (isoCodes == null || isoCodes.Length == 0)
        {
            throw new ArgumentException("At least one ISO code must be provided", nameof(isoCodes));
        }
        // Check if dateFrom is greater than dateTo
        if (startDate > endDate)
        {
            throw new ArgumentException("dateFrom cannot be greater than dateTo.");
        }

        // Get exchange rates from the database
        return await _context.ExchangeRates
            .AsNoTracking()
            .Where(r => r.Date >= startDate && r.Date <= endDate && isoCodes.Contains(r.Currency))
            .Select(r => new ExchangeRateDTO
            {
                Currency = r.Currency,
                Date = r.Date,
                Rate = r.Rate,
                Amount = r.Amount,
                Diff = r.Diff
            })
            .ToListAsync();
    }

    public async Task FetchAndSaveExchangeRates(string isoCodes, DateTime startDate, DateTime endDate)
    {
        if (string.IsNullOrWhiteSpace(isoCodes))
        {
            throw new ArgumentException("ISO codes must be provided", nameof(isoCodes));
        }

        try
        {
            var result = await _client.ExchangeRatesByDateRangeByISOAsync(isoCodes, startDate, endDate);
            var responseContent = result.Any1.InnerXml;
            var rates = ParseRates(responseContent, isoCodes);
            await SaveRates(rates, isoCodes, startDate, endDate);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error fetching exchange rates: {ex.Message}");
            throw;
        }
    }

    private List<ExchangeRate> ParseRates(string responseContent, string isoCodes)
    {
        if (string.IsNullOrWhiteSpace(responseContent))
        {
            throw new ArgumentException("Response content is empty", nameof(responseContent));
        }

        var xmlDoc = XDocument.Parse(responseContent);

        return xmlDoc.Descendants("ExchangeRatesByRange")
            .Where(x => isoCodes.Contains(x.Element("ISO")?.Value))
            .Select(Map)
            .Where(x => x != null)
            .ToList();
    }

    private async Task SaveRates(List<ExchangeRate> rates, string isoCodes, DateTime startDate, DateTime endDate)
    {
        if (rates == null || rates.Count == 0)
        {
            throw new ArgumentException("Rates must be provided", nameof(rates));
        }

        var existingRates = await _context.ExchangeRates
            .Where(r => r.Date >= startDate && r.Date <= endDate && isoCodes.Contains(r.Currency))
            .ToListAsync();

        foreach (var rate in rates)
        {
            var existingRate = existingRates.FirstOrDefault(r => r.Currency == rate.Currency && r.Date == rate.Date);
            if (existingRate != null)
            {
                existingRate.Rate = rate.Rate;
                existingRate.Amount = rate.Amount;
                existingRate.Diff = rate.Diff;
                _context.ExchangeRates.Update(existingRate);
            }
            else
            {
                _context.ExchangeRates.Add(rate);
            }
        }

        await _context.SaveChangesAsync();
    }

    private static ExchangeRate? Map(XElement element)
    {
        try
        {
            var iso = element.Element("ISO")?.Value;
            var rawRateDate = element.Element("RateDate")?.Value;
            var rawRate = element.Element("Rate")?.Value ?? "0";
            var rawAmount = element.Element("Amount")?.Value ?? "1";
            var rawDiff = element.Element("Diff")?.Value ?? "0";

            var rateDate = DateTime.Parse(rawRateDate, CultureInfo.InvariantCulture);
            var rate = decimal.Parse(rawRate, CultureInfo.InvariantCulture);
            var amount = decimal.Parse(rawAmount, CultureInfo.InvariantCulture);
            var diff = decimal.Parse(rawDiff, CultureInfo.InvariantCulture);

            return new ExchangeRate
            {
                Currency = iso,
                Date = rateDate,
                Rate = rate,
                Amount = amount,
                Diff = diff
            };
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return null;
        }
    }
}
