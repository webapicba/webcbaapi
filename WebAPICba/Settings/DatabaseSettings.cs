namespace WebAPICba.Settings;

public class DatabaseSettings
{
    public string ConnectionString { get; set; } = null!;

    public bool UseInMemoryDatabase { get; set; }
}