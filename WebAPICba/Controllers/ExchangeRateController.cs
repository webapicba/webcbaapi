using Microsoft.AspNetCore.Mvc;
using WebAPICba.Services;
using System.ComponentModel.DataAnnotations;

namespace WebAPICba.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ExchangeRateController : ControllerBase
{
    private readonly ExchangeRateService _exchangeRateService;

    // Constructor to inject dependencies
    public ExchangeRateController(ExchangeRateService exchangeRateService)
    {
        _exchangeRateService = exchangeRateService;
    }

    // API endpoint to fetch and get exchange rates
    [HttpGet("exchange-rates")]
    public async Task<IActionResult> GetAndFetchExchangeRates(
        [FromQuery][Required] DateTime dateFrom,
        [FromQuery][Required] DateTime dateTo,
        [FromQuery][Required] string iSOCodes)
    {
        // Convert isoCodes to uppercase
        iSOCodes = iSOCodes.ToUpper();

        var isoCodeArray = iSOCodes.Split(',', StringSplitOptions.RemoveEmptyEntries);

        // Calculate the total number of days requested
        var rates = await _exchangeRateService.GetExchangeRates(dateFrom, dateTo, isoCodeArray);

        int daysRequested = (dateTo - dateFrom).Days;
        int lengthiSOCodes = isoCodeArray.Length;
        int daysAll = daysRequested * lengthiSOCodes;
        int daysrates = rates.Select(r => r.Date).Distinct().Count();

        // Check if the number of days in the returned data matches the requested number of days
        if (daysrates != daysAll)
        {
            // If no rates or outdated, fetch from the API and save to the database
            await _exchangeRateService.FetchAndSaveExchangeRates(iSOCodes, dateFrom, dateTo);

            // Get the updated exchange rates from the database
            rates = await _exchangeRateService.GetExchangeRates(dateFrom, dateTo, isoCodeArray);
        }

        return Ok(rates);
       
    }
}
