﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebAPICba.Migrations
{
    public partial class AddDiffColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Diff",
                table: "ExchangeRates",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Diff",
                table: "ExchangeRates");
        }
    }
}
