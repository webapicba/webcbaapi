﻿using Microsoft.AspNetCore.Mvc;
using WebAPICba.Services;
using WebAPICba.Data;
using Microsoft.EntityFrameworkCore;
using WebAPICba.Gate;
using System.ServiceModel;
using WebAPICba.Settings;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<GateSoapClient>(serviceProvider =>
{
    var endpointAddress = new EndpointAddress(builder.Configuration.GetSection("CBAServiceSettings").GetValue<string>("EndpointAddress")); 
    var binding = new BasicHttpBinding();
    return new GateSoapClient(binding, endpointAddress);
});

// Add services to the container.
builder.Services.AddControllers()
    .ConfigureApiBehaviorOptions(options =>
    {
        options.InvalidModelStateResponseFactory = context =>
        {
            var result = new BadRequestObjectResult(context.ModelState);
            result.ContentTypes.Add("application/json");
            return result;
        };
    });

builder.Services.AddScoped<ExchangeRateService>();
var databaseSettings = builder.Configuration.GetSection("DatabaseSettings").Get<DatabaseSettings>()!;

builder.Services.AddDbContext<ExchangeRateContext>(options =>
{
    if (databaseSettings.UseInMemoryDatabase)
    {
        options.UseInMemoryDatabase("ExchangeRateDatabase");
    }
    else
    {
        options.UseSqlServer(databaseSettings.ConnectionString);
    }
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
