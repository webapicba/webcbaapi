﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPICba.Models;

// Data Transfer Object for ExchangeRate
public class ExchangeRateDTO
{
    [Required]
    [StringLength(3, MinimumLength = 3, ErrorMessage = "Currency ISO code must be 3 characters long.")]
    public string Currency { get; set; }

    [Required]
    [RegularExpression(@"^\d{4}-\d{2}-\d{2}$", ErrorMessage = "Date must be in 'Y-m-d' format.")]
    public DateTime Date { get; set; }

    [Required]
    [Range(0.01, double.MaxValue, ErrorMessage = "Rate must be greater than 0.")]
    public decimal Rate { get; set; }

    [Required]
    [Range(0.01, double.MaxValue, ErrorMessage = "Amount must be greater than 0.")]
    public decimal Amount { get; set; }

    [Required]
    [Range(0.01, double.MaxValue, ErrorMessage = "Diff must be greater than 0.")]
    public decimal Diff { get; set; }
}